# XORusingCAFFE

一个CAFFE深度学习的示例程序，关于这个例程的介绍请参考文章: [我的第一个caffe C++程序](https://blog.csdn.net/mogoweb/article/details/79784720)

例子包含两个程序，trainXOR用于训练模型，testXOR用于测试模型。

## build

实例采用了cmake构建系统，请确保系统安装了cmake。

编译方法如下:

```
mkdir build
cd build
cmake ..
make
```

## run

将上一步骤生成的二进制程序trainXOR和testXOR复制到本示例模型所在目录。

训练模型：

```
./trainXOR
```

测试模型：

```
./testXOR
```

