#include <iostream>
#include <stdlib.h>

#include <caffe/caffe.hpp>
#include <caffe/layers/memory_data_layer.hpp>

using namespace caffe;
using namespace std;

int main(int argc, char* argv[]) {

	// caffe is using google logging (aka 'glog') as its logging module, and hence this module must be initialized once when running caffe. Therefore the following line
	::google::InitGoogleLogging(argv[0]);

	// load the trained weights cached inside XOR_iter_5000000.caffemodel
	shared_ptr<Net<float> >	testnet;

	testnet.reset(new Net<float>("./model.prototxt", TEST));
	testnet->CopyTrainedLayersFrom("XOR_iter_5000000.caffemodel");

	// obtain the input MemoryData layer and pass the input to it for testing
	float testab[] = {0, 0, 0, 1, 1, 0, 1, 1};
	float testc[] = {0, 1, 1, 0};

	MemoryDataLayer<float> *dataLayer_testnet = (MemoryDataLayer<float> *)(testnet->layer_by_name("test_inputdata").get());

	dataLayer_testnet->Reset(testab, testc, 4);

	// calculate the neural network output
	testnet->Forward();

	// access blobs to display results
	boost::shared_ptr<Blob<float> > output_layer = testnet->blob_by_name("output");

	const float* begin = output_layer->cpu_data();
	const float* end = begin + 4;

	// We know the output size is 4, and we save the outputs into the result vector
	vector<float> result(begin, end);

	// display the result
	for (int i = 0; i < result.size(); i++) {
		cout << "input: " << (int)testab[i * 2 + 0]
		<< " xor " << (int)testab[i * 2 + 1]
		<< ", truth: " << (int)testc[i]
		<< ", result by NN: " << result[i] << endl;
	}

	return(0);
}
