#include <jni.h>
#include <string>

#include <android/log.h>

#include <caffe/caffe.hpp>
#include <caffe/layers/memory_data_layer.hpp>

using namespace caffe;

string jstring2string(JNIEnv *env, jstring jstr) {
    const char *cstr = env->GetStringUTFChars(jstr, 0);
    string str(cstr);
    env->ReleaseStringUTFChars(jstr, cstr);
    return str;
}

jstring string2jstring(JNIEnv * env, const std::string & nativeString) {
    return env->NewStringUTF(nativeString.c_str());
}

extern "C" JNIEXPORT jstring

JNICALL
Java_com_mogoweb_caffe_xorusingcaffe_MainActivity_startXORTest(
        JNIEnv *env,
        jobject /* this */,
        jstring modelProto,
        jstring caffeModel) {
    // caffe is using google logging (aka 'glog') as its logging module, and hence this module must be initialized once when running caffe.
    // Therefore the following line
    ::google::InitGoogleLogging("");

    // load the trained weights cached inside XOR_iter_5000000.caffemodel
    shared_ptr<Net<float> >	testnet;

    std::string modelproto = jstring2string(env, modelProto);
    std::string caffemodel = jstring2string(env, caffeModel);
    __android_log_print(ANDROID_LOG_INFO, "XOR", "modelproto:%s, caffemodel:%s", modelproto.c_str(), caffemodel.c_str());
    testnet.reset(new Net<float>(modelproto, TEST));
    testnet->CopyTrainedLayersFrom(caffemodel);

    // obtain the input MemoryData layer and pass the input to it for testing
    float testab[] = {0, 0, 0, 1, 1, 0, 1, 1};
    float testc[] = {0, 1, 1, 0};

    MemoryDataLayer<float> *dataLayer_testnet = (MemoryDataLayer<float> *)(testnet->layer_by_name("test_inputdata").get());

    dataLayer_testnet->Reset(testab, testc, 4);

    // calculate the neural network output
    testnet->Forward();

    // access blobs to display results
    boost::shared_ptr<Blob<float> > output_layer = testnet->blob_by_name("output");

    const float* begin = output_layer->cpu_data();
    const float* end = begin + 4;

    // We know the output size is 4, and we save the outputs into the result vector
    vector<float> result(begin, end);

    // display the result
    char buf[512] = {0};
    string s;
    for (int i = 0; i < result.size(); i++) {
        sprintf(buf, "input: %d xor %d, truth: %d, result by NN: %f\n",
                (int)testab[i * 2 + 0], (int)testab[i * 2 + 1], (int)testc[i],result[i]);
        s += buf;
    }

    __android_log_print(ANDROID_LOG_INFO, "XOR", "test result:%s", s.c_str());
    return string2jstring(env, s);
}
