# XORusingCAFFE-android
一个CAFFE深度学习的Android示例程序，关于这个例程的介绍请参考文章: [我的第一个caffe Android程序](https://blog.csdn.net/mogoweb/article/details/79796713)

例子用来用于测试训练好了的模型。

## build

请使用android studio打开工程，并build出apk，安装到Android手机上。

## run

先将模型push到Android手机的sdcard:

```
adb push app/src/main/model/model.prototxt /sdcard/
adb push app/src/main/model/XOR_iter_5000000.caffemodel /sdcard/
```

然后启动应用程序，点击**Start test**按钮
