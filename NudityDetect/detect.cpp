#include <caffe/caffe.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>

using namespace caffe;  // NOLINT(build/namespaces)
using std::vector;
using std::string;
using std::cout;
using std::endl;

class Classifier {
public:
    Classifier(const string& model_file,
              const string& trained_file);
    std::vector<float> Predict(const cv::Mat& img);

private:
    void SetMean(const vector<float> &mean_values);

    void WrapInputLayer(std::vector<cv::Mat>* input_channels);

    void Preprocess(const cv::Mat& img,
                    std::vector<cv::Mat>* input_channels);

private:
    shared_ptr<Net<float> > net_;
    cv::Size input_geometry_;
    int num_channels_;
    cv::Mat mean_;
};

Classifier::Classifier(const string& model_file,
                       const string& trained_file) {
    cout << "construct Classifier" << endl;
#ifdef CPU_ONLY
    Caffe::set_mode(Caffe::CPU);
#else
    Caffe::set_mode(Caffe::GPU);
#endif

    /* Load the network. */
    net_.reset(new Net<float>(model_file, TEST));
    net_->CopyTrainedLayersFrom(trained_file);

    CHECK_EQ(net_->num_inputs(), 1) << "Network should have exactly one input.";
    CHECK_EQ(net_->num_outputs(), 1) << "Network should have exactly one output.";

    Blob<float>* input_layer = net_->input_blobs()[0];
    num_channels_ = input_layer->channels();
    CHECK(num_channels_ == 3)
            << "Input layer should have 3 channels.";
    input_geometry_ = cv::Size(input_layer->width(), input_layer->height());
    cout << "input_geometry_: " << input_geometry_.width << "x" << input_geometry_.height << endl;

    vector<float> mean_values = {104, 117, 123};
    SetMean(mean_values);
}

void Classifier::SetMean(const vector<float> &mean_values) {
    CHECK_EQ(mean_values.size(), num_channels_)
            << "Number of mean values doesn't match channels of input layer.";

    cv::Scalar channel_mean(0);
    double *ptr = &channel_mean[0];
    for (int i = 0; i < num_channels_; ++i) {
        ptr[i] = mean_values[i];
    }
    mean_ = cv::Mat(input_geometry_, (num_channels_ == 3 ? CV_32FC3 : CV_32FC1), channel_mean);
}

std::vector<float> Classifier::Predict(const cv::Mat& img) {
    cout << "start Predict ..." << endl;
    Blob<float>* input_layer = net_->input_blobs()[0];
    input_layer->Reshape(1, num_channels_,
                       input_geometry_.height, input_geometry_.width);
    /* Forward dimension change to all layers. */
    net_->Reshape();

    cout << "WrapInputLayer ..." << endl;
    std::vector<cv::Mat> input_channels;
    WrapInputLayer(&input_channels);

    cout << "Preprocess ..." << endl;
    Preprocess(img, &input_channels);

    cout << "Forward ..." << endl;
    net_->Forward();

    cout << "get result" << endl;
    /* Copy the output layer to a std::vector */
    Blob<float>* output_layer = net_->output_blobs()[0];
    const float* begin = output_layer->cpu_data();
    const float* end = begin + output_layer->channels();
    return std::vector<float>(begin, end);
}

/* Wrap the input layer of the network in separate cv::Mat objects
 * (one per channel). This way we save one memcpy operation and we
 * don't need to rely on cudaMemcpy2D. The last preprocessing
 * operation will write the separate channels directly to the input
 * layer. */
void Classifier::WrapInputLayer(std::vector<cv::Mat>* input_channels) {
    Blob<float>* input_layer = net_->input_blobs()[0];

    int width = input_layer->width();
    int height = input_layer->height();
    float* input_data = input_layer->mutable_cpu_data();
    for (int i = 0; i < input_layer->channels(); ++i) {
        cv::Mat channel(height, width, CV_32FC1, input_data);
        input_channels->push_back(channel);
        input_data += width * height;
    }
}

void Classifier::Preprocess(const cv::Mat& img,
                            std::vector<cv::Mat>* input_channels) {
    /* Convert the input image to the input image format of the network. */
    cv::Mat sample;
    cout << "img channels:" << img.channels() << ", num_channels_:" << num_channels_ << endl;
    if (img.channels() == 4 && num_channels_ == 3)
        cv::cvtColor(img, sample, cv::COLOR_BGRA2BGR);
    else if (img.channels() == 1 && num_channels_ == 3)
        cv::cvtColor(img, sample, cv::COLOR_GRAY2BGR);
    else
        sample = img;

    cv::Mat sample_resized;
    if (sample.size() != input_geometry_) {
        cout << "crop ..." << endl;
        // crop image
        cv::Size size = sample.size();
        int H = size.height;
        int W = size.width;
        int h = input_geometry_.height;
        int w = input_geometry_.width;

        int h_off = std::max((H - h) / 2, 0);
        int w_off = std::max((W - w) / 2, 0);
        sample(cv::Rect(w_off, h_off, w, h)).copyTo(sample_resized);
    }
    else
        sample_resized = sample;

    cout << "convert ..." << endl;
    cv::Mat sample_float;
    sample_resized.convertTo(sample_float, CV_32FC3);

    cout << "subtract ..." << endl;
    cv::Mat sample_normalized;
    cv::subtract(sample_float, mean_, sample_normalized);

    cout << "sample_normalized size:" << sample_normalized.size() << endl;

    cout << "split ..." << endl;
    /* This operation will write the separate BGR planes directly to the
     * input layer of the network because it is wrapped by the cv::Mat
     * objects in input_channels. */
    cv::split(sample_normalized, *input_channels);

    CHECK(reinterpret_cast<float*>(input_channels->at(0).data) == net_->input_blobs()[0]->cpu_data())
            << "Input channels are not wrapping the input layer of the network.";
}

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " img.jpg" << std::endl;
        return 1;
    }

    ::google::InitGoogleLogging(argv[0]);

    string model_file   = "./nsfw_model/deploy.prototxt";
    string trained_file = "./nsfw_model/resnet_50_1by2_nsfw.caffemodel";
    Classifier classifier(model_file, trained_file);

    string file = argv[1];

    cout << "read image file ..." << endl;
    cv::Mat img = ReadImageToCVMat(file, 256, 256);
    CHECK(!img.empty()) << "Unable to decode image " << file;
    cout << "image channels:" << img.channels() << " size:" << img.size() << endl;

    std::vector<float> predictions = classifier.Predict(img);

    std::cout << "NSFW score:  " << std::fixed << std::setprecision(4) << predictions[1] << std::endl;
}
